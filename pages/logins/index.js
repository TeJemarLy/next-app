import { useState, useContext } from 'react';
import { Form, Button, Container} from 'react-bootstrap';
import UserContext from '../../UserContext';
import usersData from '../../data/users';
import Router from 'next/router';

export default function index(){
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPasword] = useState('')

	useEffect(() => {
		console.log(`User with email: ${user.email} is an Admin ${user.isAdmin}`)
	}, [user.isAdmin, user.email])

	function authenticate(e){
		e.preventDefault();

		const match = userData.find(user => {
			return (userData.email === email && userData.password === password)
		})

		if(match){
			localStorage.setItem('email', email)
			localStorage.setItem('isAdmin', match.isAdmin)

			setUser({
				email: localStorage.getItem('email')
				isAdmin: match.isAdmin
			})

			alert('Successfully logged in')
			Router.push('/courses')
		}else{
			alert('Authentication Failed')
		}

	}
}